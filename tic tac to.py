t=[" "]*9 
from random import randrange
#==========================================
def check_winner(n):
    if (t[0]==t[1]==t[2]==n) or (t[3]==t[4]==t[5]==n) or (t[6]==t[7]==t[8]==n):
        return True
    if (t[0]==t[3]==t[6]==n) or (t[1]==t[4]==t[7]==n) or (t[2]==t[5]==t[8]==n):
        return True
    if (t[0]==t[4]==t[8]==n) or (t[2]==t[4]==t[6]==n):
        return True
    return False
#==========================================
def check():
    if check_winner('X') == True:
        print('Winner! GG')
        return True
    elif check_winner('O') == True:
        print('GAME OVER! you lost')
        return True
    else:
        return False
#==========================================
def draw():
    print(" {} | {} | {} ".format(t[0],t[1],t[2]))
    print('--- --- --- ')
    print(" {} | {} | {} ".format(t[3],t[4],t[5]))
    print('--- --- --- ')
    print(" {} | {} | {} ".format(t[6],t[7],t[8]))
#==========================================
def check_input(x):
    if '0' <= x <= '8':
        if t[int(x)] == ' ' : return True
        else                : return False
    else:
        return False
#==========================================
def user():
    while True:
        a = input('Enter (0 to 8) : ')
        if check_input(a) == True:
            break
    t[int(a)] = 'X'
#==========================================
def AI_random():
    while True:
        x = randrange(0,9)
        if t[x] == ' ':
            t[x] = 'O'
            break
#==========================================
def AI_attack():
    for i in range(9):
        if t[i] == ' ':
            t[i] = 'O'
            if check_winner('O') == True:
                return True
            t[i] = ' '
    return False
#==========================================
def AI_defence():
    for i in range(9):
        if t[i] == ' ':
            t[i] = 'X'
            if check_winner('X') == True:
                t[i] = 'O'
                return True
            t[i] = ' '
    return False
#==========================================
def AI():
    if AI_attack() == False :
        if AI_defence() == False :
            AI_random()
#==========================================
def start():
    dio = input('write start and then enter')
    if dio=='start' :
        return True
#==========================================
t=[0,1,2,3,4,5,6,7,8]
draw()
t=[" "]*9 
if start()==True:
 while True:
     draw()
     if check() == True : break
     user()
     draw()
     if check() == True : break
     AI()
else:
    start()
